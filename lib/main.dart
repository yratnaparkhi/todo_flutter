import 'package:flutter/material.dart';
import 'package:my_todo_app/utilities/NewTask.dart';
import 'package:my_todo_app/utilities/Task_display.dart';

import 'models/tasks.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Todo(),
    );
  }
}

class Todo extends StatefulWidget {
  const Todo({Key? key}) : super(key: key);

  @override
  State<Todo> createState() => _TodoState();
}

class _TodoState extends State<Todo> {
  final List<Tasks> totalTasks = [];
  String? dueDate;
  bool? checked = false;
  void newTask(BuildContext ctx, isLandscape) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top:Radius.circular(40)),
      ),
      backgroundColor: const Color(0xFF78B5C0),
      context: ctx,
      builder: (_) {
        return GestureDetector(
          onTap: () {},
          child: NewTask(addNewTask,isLandscape),
        );
      },
    );
  }

  void addNewTask(String indexId, String taskTitle, DateTime chosenDate) {
    final tasks = Tasks(id: indexId, name: taskTitle, time: chosenDate);

    setState(() {
      totalTasks.add(tasks);
    });
  }

  @override
  Widget build(BuildContext context) {
    bool _isLandscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xFF78B5C0),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                height: MediaQuery.of(context).size.height * .1,
                width: MediaQuery.of(context).size.width * .5,
                margin:
                    const EdgeInsets.symmetric(horizontal: 50, vertical: 50),
                padding: const EdgeInsets.all(8),
                child: const FittedBox(
                  child: Text(
                    "DAILY TASKS",
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * .05,
              margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: const FittedBox(
                child: Text(
                  "Upcoming",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                  textAlign: TextAlign.start,
                ),
              ),
            ),
            _isLandscape
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "Show Tasks",
                        style: TextStyle(color: Colors.white,fontSize: 20),
                      ),
                      Switch(
                          value: checked!,
                          onChanged: (val) {
                            setState(() {
                              checked = val;
                            });
                          }),
                    ],
                  )
                : Container(),
                !_isLandscape?
                 SizedBox(
                    height: MediaQuery.of(context).size.height * .61,
                    child: totalTasks.isEmpty
                        ? Container(
                            margin: EdgeInsets.symmetric(
                                vertical:
                                    (MediaQuery.of(context).size.height * .02)),
                            child: Image.asset("assets/empty.png", height: 130))
                        : TasksDisplay(
                            totalTasks: totalTasks,
                          ),
                    width: double.infinity,
                  )
                :  !checked!?
                Container(
                    margin: const EdgeInsets.only(top:20),
                    child: const Center(
                      child: Text(
                        "Enable show task to check for your tasks",
                        style: TextStyle(color: Colors.white,fontSize: 20),
                      ),
                    ),
                  ):SizedBox(
                  height: MediaQuery.of(context).size.height * .61,
                  child: totalTasks.isEmpty
                      ? Container(
                      margin: EdgeInsets.symmetric(
                          vertical:
                          (MediaQuery.of(context).size.height * .02)),
                      child: Image.asset("assets/empty.png", height: 130))
                      : TasksDisplay(
                    totalTasks: totalTasks,
                  ),
                  width: double.infinity,
                ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        child: SizedBox(
          height: MediaQuery.of(context).size.height * .3,
          child: const Icon(
            Icons.add,
            color: Color(0xFF78B5C0),
          ),
        ),
        onPressed: () => newTask(context,_isLandscape),
      ),
    );
  }
}
