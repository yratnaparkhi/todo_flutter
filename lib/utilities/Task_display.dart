import 'package:my_todo_app/models/tasks.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
class TasksDisplay extends StatefulWidget {
 const TasksDisplay({Key? key, required this.totalTasks}) : super(key: key);

  final List<Tasks> totalTasks;

  @override
  State<TasksDisplay> createState() => _TasksDisplayState();
}

class _TasksDisplayState extends State<TasksDisplay> {


  void deleteTask(String id) {
    setState(() {
      widget.totalTasks.removeWhere((tx) => tx.id == id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.totalTasks.length,
        itemBuilder: (ctx, index) {
          var name = widget.totalTasks[index].name;
          var time = widget.totalTasks[index].time;
          return Card(
            elevation: 5,
            margin: const EdgeInsets.symmetric(
              vertical: 8,
              horizontal: 20,
            ),
            child: ListTile(
              leading: Padding(
                padding: const EdgeInsets.fromLTRB(12, 10, 8, 8.0),
                child: Text(
                  (index + 1).toString(),
                  style: const TextStyle(fontSize: 18),
                ),
              ),
              title: Padding(
                padding: const EdgeInsets.only(left: 30.0),
                child: Text(name),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(left: 30.0),
                child: Text(
                  DateFormat.yMMMd().format(time),
                ),
              ),
              trailing: IconButton(
                icon: const Icon(Icons.delete),
                color: Theme.of(context).errorColor,
                onPressed: () => deleteTask(widget.totalTasks[index].id),
              ),
            ),
          );
        });
  }
}


