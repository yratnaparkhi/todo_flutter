import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTask extends StatefulWidget {
  final Function addTask;
  final bool oriention_landscape;
  NewTask(this.addTask,this.oriention_landscape);

  @override
  State<NewTask> createState() => _NewTaskState();
}

class _NewTaskState extends State<NewTask> {
  final taskController = TextEditingController();
  final dateController = TextEditingController();
  DateTime? datePicked;
  void showDate() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2023),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        datePicked = pickedDate;
      });
    });
  }
@override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    String date = datePicked == null
        ? 'No Date Chosen!'
        : 'Picked Date: ${DateFormat.yMd().format(datePicked!)}';

    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30),
      height: widget.oriention_landscape? screenHeight :screenHeight * .5,
      child: SingleChildScrollView(
        child: Column(
          children: [
             SizedBox(
              height: screenHeight * .05,
            ),
            const Text("Enter new Task",
                style: TextStyle(fontSize: 28, color: Colors.white)),
             SizedBox(
              height: screenHeight * .03,
            ),
            Container(
                height: screenHeight * .07,
              color: Colors.white,
              child: TextField(
                decoration: const InputDecoration(
                  label: Text('Todo'),
                  focusedBorder: OutlineInputBorder(),
                  border: OutlineInputBorder(), // border: Border.all()
                ),
                controller: taskController,
              ),
            ),
             SizedBox(
              height: screenHeight * .03,
            ),
            Container(
              color: Colors.white,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              height: screenHeight * .07,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(date),
                  ),
                  FlatButton(
                    textColor: Theme.of(context).primaryColor,
                    child: const Text(
                      'Choose Date',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: showDate,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight * .03,
            ),
            Container(
                height: screenHeight * .06,
              width: 80,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 2)),
              child: FlatButton(
                color: const Color(0xFF78B5C0),
                onPressed: () {
                  setState(() {
                    widget.addTask(
                      DateTime.now().toString(),
                      taskController.text,
                      datePicked!,
                    );
                    taskController.text = '';
                    datePicked = null;
                    Navigator.pop(context);
                  });
                },
                child: const Text(
                  'add',
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
